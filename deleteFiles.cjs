const fsPromises = require("fs/promises");
const path = require("path");

function deleteFiles(directoryName, deleteFilesList) {
  return new Promise((resolve, reject) => {
    if (deleteFilesList.length === 0) {
      reject("No such files to delete");
    } else {
      let deleteGivenFiles = deleteFilesList.map((eachFile) => {
        let filePath = path.join(
          __dirname + "/userFolder" + `/${directoryName}`,
          eachFile
        );
        return fsPromises.unlink(filePath);
      });
      resolve(deleteGivenFiles);
    }
  });
}

module.exports = deleteFiles;
