const fsPromises = require("fs/promises");
const path = require("path");

function writeJsonFiles(directoryName, filesList) {
  return new Promise((resolve, reject) => {
    if (filesList.length === 0) {
      reject("All files are exist already");
    } else {
      let writeAllFiles = filesList.map((eachFile) => {
        let filePath = path.join(
          __dirname + "/userFolder" + `/${directoryName}`,
          eachFile
        );

        return fsPromises.writeFile(filePath, "");
      });
      resolve(writeAllFiles);
    }
  });
}

module.exports = writeJsonFiles;
