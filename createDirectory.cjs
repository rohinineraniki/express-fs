const fsPromises = require("fs/promises");
const path = require("path");

function createDirectory(directoryName, res) {
  let filePath = path.join(__dirname + "/userFolder", directoryName);
  return fsPromises.mkdir(filePath, { recursive: true });
}

module.exports = createDirectory;
