const express = require("express");
const createError = require("http-errors");

const router = require("./index.cjs");

const app = express();
const PORT = process.env.PORT || 8000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/", router);

app.use((req, res, next) => {
  next(createError(404));
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({ message: "Error" });
});

app.listen(PORT, () => {
  console.log("listening 8000");
});
