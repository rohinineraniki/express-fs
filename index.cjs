const express = require("express");

const createDirectory = require("./createDirectory.cjs");
const deleteFiles = require("./deleteFiles.cjs");
const writeJsonFiles = require("./writeJsonFiles.cjs");
const readFiles = require("./readFiles.cjs");

const router = express.Router();

function getFilesToCreate(filesList, data) {
  let filesToCreate = filesList.filter((eachFile) => {
    if (!data.includes(eachFile) && !eachFile.includes("/")) {
      return true;
    }
  });
  return filesToCreate;
}

function getFilesNotToCreate(filesList, data) {
  let filesNotToCreate = filesList.filter((eachFile) => {
    if (data.includes(eachFile) || eachFile.includes("/")) {
      return true;
    }
  });
  return filesNotToCreate;
}

router.post("/", (req, res, next) => {
  let { directoryName, filesList } = req.body;

  let resultObject = {
    status: "",
    message: "",
    messageToUser:
      "Directory or file which contain / will not create or if given file already exists then new file will not create",
    filesNotCreated: "",
    filesCreated: "",
  };

  if (
    directoryName.length === 0 ||
    directoryName === undefined ||
    filesList === undefined ||
    filesList.length === 0 ||
    !Array.isArray(filesList)
  ) {
    resultObject.status = "Error";
    resultObject.message = "Please give valid body inputs";

    res.status(400).json(resultObject);
  } else {
    if (req.body.directoryName.includes("/")) {
      resultObject.status = "Error";
      resultObject.message = "Directory name does not contain / ";

      res.status(403).json(resultObject);
    } else {
      createDirectory(directoryName)
        .then((data) => {
          return readFiles(directoryName);
        })
        .then((data) => {
          let filesToCreate = getFilesToCreate(filesList, data);
          let filesNotToCreate = getFilesNotToCreate(filesList, data);

          resultObject.filesNotCreated = filesNotToCreate;
          resultObject.filesCreated = filesToCreate;
          return writeJsonFiles(directoryName, filesToCreate);
        })
        .then((data) => {
          console.log(data);
          return Promise.allSettled(data);
        })
        .then((data) => {
          console.log(data);
          let filesCreated = resultObject.filesCreated;
          let filesNotCreated = resultObject.filesNotCreated;

          let finalFiles = data.reduce((acc, each, index) => {
            if (each.status === "fulfilled") {
              acc.push(filesCreated[index]);
              return acc;
            } else {
              filesNotCreated.push(filesCreated[index]);
            }
            return acc;
          }, []);

          resultObject.status = "Success";
          resultObject.message = "Created directory and files successfully";
          resultObject.filesCreated = finalFiles;
          resultObject.filesNotCreated = filesNotCreated;

          res.json({
            resultObject,
          });
        })
        .catch((err) => {
          console.log(err);
          if (err === "All files are exist already") {
            resultObject.status = "Error";
            resultObject.message =
              "All files are exist already,Please give a new files";
            res.status(422).json({
              resultObject,
            });
          } else {
            next(err);
          }
        });
    }
  }
});

function getFilesTobeDeleted(deleteFilesList, data) {
  let filesTobeDeleted = deleteFilesList.filter((eachFile) => {
    if (data.includes(eachFile) && !eachFile.includes("/")) {
      return true;
    }
  });
  return filesTobeDeleted;
}

function getFilesNotToDelete(deleteFilesList, data) {
  let filesNotDelete = deleteFilesList.filter((eachFile) => {
    if (data.includes(eachFile) || !eachFile.includes("/")) {
      return true;
    }
  });
  return filesNotDelete;
}

router.delete("/", (req, res, next) => {
  let { directoryName, deleteFilesList } = req.body;

  let result = {
    status: "",
    message: "",
    messageToUser:
      "Directory or file which contain / will not delete and if given file not exists in given directory unable to delete it",
    deletedFiles: "",
    notDeletedFiles: "",
  };

  if (
    directoryName === undefined ||
    deleteFilesList === undefined ||
    !Array.isArray(deleteFilesList)
  ) {
    result.status = "Error";
    result.message = "Please give valid body inputs";

    res.status(400).json(result);
  } else {
    if (req.body.directoryName.includes("/")) {
      result.status = "Error";
      result.message = "Directory name does not contain / ";

      res.status(403).json(result);
    } else {
      if (deleteFilesList.length > 0) {
        readFiles(directoryName)
          .then((data) => {
            let filesTobeDeleted = getFilesTobeDeleted(deleteFilesList, data);
            let filesNotDelete = getFilesNotToDelete(deleteFilesList, data);

            result.deletedFiles = filesTobeDeleted;
            result.notDeletedFiles = filesNotDelete;
            return deleteFiles(directoryName, filesTobeDeleted);
          })
          .then((data) => {
            return Promise.allSettled(data);
          })
          .then((data) => {
            let deletedFiles = result.deletedFiles;
            let notDeletedFiles = result.notDeletedFiles;
            let finalFiles = data.reduce((acc, each, index) => {
              if (each.status === "fulfilled") {
                acc.push(deletedFiles[index]);
                return acc;
              } else {
                notDeletedFiles.push(deletedFiles[index]);
              }
              return acc;
            }, []);

            result.status = "Success";
            result.message = "Files are deleted successfully";
            result.deletedFiles = finalFiles;
            result.notDeletedFiles = notDeletedFiles;

            res.json(result).end();
          })
          .catch((err) => {
            console.log(err);
            if (err === "No such files to delete") {
              result.status = "Error";
              result.message = "No such files to delete";

              res.status(422).json(result).end();
            } else if (err === "File name does not include /") {
              result.status = "Error";
              result.message = "File name does not include /";

              res.status(422).json(result);
            } else {
              next(err);
            }
          });
      } else {
        result.status = "Error";
        result.message = "Please enter file names to be deleted";
        res.status(422).json(result);
      }
    }
  }
});

module.exports = router;
