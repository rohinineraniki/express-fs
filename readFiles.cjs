const fsPromises = require("fs/promises");
const path = require("path");

function readFiles(directoryName) {
  let filePath = path.join(__dirname + "/userFolder", directoryName);
  return fsPromises.readdir(filePath);
}

module.exports = readFiles;
